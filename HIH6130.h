/*
HIH-6130 I2C control library for ARDUINO
This is an extremely simple interface allowing user query of humidity and temp buffers. No further functionality implemented.

Hookup:
	Arduino					HIH-6130
	SCL(Analog5) 	----	SCL
	SDA(Analog4) 	----   	SDA
	5V			 	----	Vdd
	GND				----	GND
	
Written by: Thomas Davies, August 2015
*/

#ifndef _HIH6130_h_included_
#define _HIH6130_h_included_

class HIH6130 {
	HIH6130();
	
	public:
		//simple abstracted interface for user.
		void refreshReadings();
		float getTempInCelcius();
		float getHumidityInRH();
		
	private:
		void fetchHumidityTemperature(unsigned int *p_H_dat, unsigned int *p_T_dat);
		float humidityInRH;
		float temperatureInC;
};

#endif
   

   
 
