#include "HIH6130.h"
#include <Wire.h>
#include "Arduino.h"

HIH6130::HIH6130() {
	//do nothing
}

float HIH6130::getTempInCelcius() {
	return HIH6130::temperatureInC;
}

float HIH6130::getHumidityInRH(){
	return HIH6130::humidityInRH;
};

//update data for user. Must be called if user wants updated values for getTemp and getHumidity accessors.
void HIH6130::refreshReadings() {
	unsigned int humidityBuffer, temperatureBuffer;
	fetchHumidityTemperature(&humidityBuffer,&temperatureBuffer);
	
	HIH6130::humidityInRH = (float) humidityBuffer * 6.10e-3;
	HIH6130::temperatureInC = (float) temperatureBuffer * 1.007e-2 - 40.0;
};

	
void HIH6130::fetchHumidityTemperature(unsigned int *p_H_dat, unsigned int *p_T_dat)
{
	byte address, Hum_H, Hum_L, Temp_H, Temp_L;	 //,_status;
	unsigned int H_dat, T_dat;
	address = 0x27;;
	Wire.beginTransmission(address); 
	Wire.endTransmission();
	delay(100);

	Wire.requestFrom((int)address, (int) 4);
	Hum_H = Wire.read();
	Hum_L = Wire.read();
	Temp_H = Wire.read();
	Temp_L = Wire.read();
	Wire.endTransmission();

	//_status = (Hum_H >> 6) & 0x03;
	Hum_H = Hum_H & 0x3f;
	H_dat = (((unsigned int)Hum_H) << 8) | Hum_L;
	T_dat = (((unsigned int)Temp_H) << 8) | Temp_L;
	T_dat = T_dat / 4;
	
	*p_H_dat = H_dat;
  *p_T_dat = T_dat;
};
