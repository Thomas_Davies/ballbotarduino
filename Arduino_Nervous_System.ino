#include <Wire.h>
#include <ArduinoJson.h>
#include "Motor.h"
#include "HIH6130.h"

////// PIN DEFINES ///////
#define pMotor1Dir  12
#define pMotor1En   11 //PWM
#define pMotor2Dir  8
#define pMotor2En   9 //PWM
#define pMotor3Dir  7
#define pMotor3En   6  //pwm
#define pMotor4Dir  4
#define pMotor4En   3  //pwm


bool isNewRequest = false;
int rotation = 0;

//Serial port tests
//    motors: "{"direction": 3.92,"speed": 100}"


void setup() {
  Serial.begin(115200); 
  pinMode(pMotor1Dir,OUTPUT);
  pinMode(pMotor1En,OUTPUT);
  pinMode(pMotor2Dir,OUTPUT);
  pinMode(pMotor2En,OUTPUT);
  pinMode(pMotor3Dir,OUTPUT);
  pinMode(pMotor3En,OUTPUT);
  pinMode(pMotor4Dir,OUTPUT);
  pinMode(pMotor4En,OUTPUT);    

}

void loop() {
  int curlyCounter = -1;
  char inString[200];
  int i = 0;

  //Grab data frin the serial buffer 1 byte at a time, base collection of # of { and } parsed!
  if (Serial.available())
  {
    bool isStart = 0;
    
    while (curlyCounter != 0)
    {      
      if (Serial.available() > 0) {
        char c = Serial.read();  //get one byte from serial buffer
        if (c == '{')
        {
          if (!isStart) {
            isStart = 1;
            curlyCounter = 0;
          }
          curlyCounter ++;
        }
        else if (c == '}')
        {
          curlyCounter --;
        }        
        if(isStart)
          inString[i++] = c;
      }
      isNewRequest = true;
    }   
    inString[i++] = '\0';
  }

  if (isNewRequest)
  {     

    StaticJsonBuffer<800> jsonBuffer;
    JsonObject& request = jsonBuffer.parseObject(inString);

    float dir = request["direction"];
    int mag = request["speed"];
    
    setMotorSpeeds(dir,mag);   
    
    //TODO: hookup sensors with requests. Do for after first demo.
    //Sensor grabs//
    if (request["sensors"]["temperature"]) {
      //grab temp data

      //format data and add to json
    }

    if (request["sensors"]["humidity"]){
      //grab humidity data

      //format data and add to json
    }

    if (request["sensors"]["light"]){
      //grab light data
      
      //format data and add to json
    }    
    //END OF TODO
    
    inString[0] = 0;
    isNewRequest = false;
  }
}

void setMotorSpeeds(double heading,double mag)
{
  
  //using trig functions may prove to be an issue. They are slow as fuck. May implement trig look-up table.
  float motor1_speed = mag*sin( heading - PI/4)   - rotation;
  float motor2_speed = mag*sin( heading - 3*PI/4) - rotation;
  float motor3_speed = mag*sin( heading - 5*PI/4) - rotation;
  float motor4_speed = mag*sin( heading - 7*PI/4) - rotation;   

  //need to add some level of acceleration to avoid instanteous high torque case.
  

  digitalWrite(pMotor1Dir, motorDirection(motor1_speed));
  analogWrite(pMotor1En,abs(motor1_speed));

  digitalWrite(pMotor2Dir, motorDirection(motor2_speed));
  analogWrite(pMotor2En,abs(motor2_speed));

  digitalWrite(pMotor3Dir, motorDirection(motor3_speed));
  analogWrite(pMotor3En,abs(motor3_speed));

  digitalWrite(pMotor4Dir, motorDirection(motor4_speed));
  analogWrite(pMotor4En,abs(motor4_speed));

}

int motorDirection (float Velocity) {
  if (Velocity > 0)
    return 1;
  else
    return 0;
}








